<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Auth;

class PagesController extends Controller
{
    public function index() {

    	return view('index');
    }

    public function gallery() {

    	return view('gallery');
    }

     public function login() {

    	return view('login');
	}

	public function checkLogin(Request $request) {

		$data_user = array (
			'contact' => $request->input('contact'),
			'password' => $request->input('password'),
		);
		$error = "WRONG INPUT";
		if(Auth::attempt($data_user)) {

			return redirect('users/index');
			
		} else {

			return redirect('login')->with('error', $error);
		}

	}
}
