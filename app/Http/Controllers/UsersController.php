<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'fname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if ($validatedData) {

            $users = new User;
            // users::create(Request::all());
            $users->fname = $request->input('fname');
            $users->lname = $request->input('lname');
            $users->email = $request->input('email');
            $users->password = Hash::make($request->input('password'));
            $users->contact = $request->input('contact');
            $users->country = $request->input('country');
            $users->city = $request->input('city');
            $users->add_info = $request->input('addInfo');
            $users->user_type = $request->input('userType');


            $users->save();
        }

        return redirect('/users');
        // return '12122';
    }

}
