@extends('layouts.app')

@section('content')

    <header>
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">

            <div class="cs-homeTitle">
                <h1 style="font-size: 10rem; font-family: cursive;">UBEC CARS</h1>  
            </div>         

          <!-- Slide One - Set the background image for this slide in the line below -->
          <div class="carousel-item active" style="background-image: url({{URL::asset('/images/car1.jpg')}})">
            <div class="carousel-caption d-none d-md-block">
              <h3>Suzuki</h3>
              <p>Suzuki Escudo (150k)</p>
            </div>
          </div>
          <!-- Slide Two - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url({{URL::asset('/images/car2.jpg')}})">
            <div class="carousel-caption d-none d-md-block">
              <h3>SUZUKI</h3>
              <p>Matic 4x4 (Gas).</p>
            </div>
          </div>
          <!-- Slide Three - Set the background image for this slide in the line below -->
          <div class="carousel-item" style="background-image: url({{URL::asset('/images/car3.jpg')}})">
            <div class="carousel-caption d-none d-md-block">
              <h3>Hyundai Accent</h3>
              <p>Manual Turbo Diesel 2007 (250k) .</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>
<br><br>
  

@endsection