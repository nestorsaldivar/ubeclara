<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>

        <link rel="stylesheet" type="text/css" href="{{asset('css/customcss.css')}}">
        <script type="text/javascript" src="{{asset('js/customjs.js')}}"></script>
        <!-- Styles -->

    </head>
    <body>
        <nav class="header navbar navbar-expand-lg navbar-dark bg-dark">
          @if(Auth::check())
          <a class="navbar-brand" href="/">Hi {{ Auth::user()->fname }}</a>
          @else
          <a class="navbar-brand" href="/">UBEC</a>
          @endif
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                <a class="nav-link" href="/">Home</a>
              </li>
              @if(Auth::check())
                <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                 <a class="nav-link" href="{{ URL('users') }}">Users</a>
                </li>
              @else
              
              @endif
              <li class="nav-item {{ Request::is('register') ? 'active' : '' }}">
                <a class="nav-link" href="{{ URL('/users/create') }}">Register</a>
              </li>
            </ul>
            <ul class="navbar-nav ml-auto">

              @if(Auth::user())
                <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                </li>
                @else
                <li class="nav-item {{ Request::is('login') ? 'active' : '' }}">
                  <a class="nav-link" href="{{ route('login') }}">Login</a>
                </li>
              @endif
           </ul>
          </div>
        </nav>

        @yield('content')
    
    <footer class="card-footer" >UBEC Copyright 2018</footer>
     </body>
</html>
