@extends('layout.app')

@section('content')

	<div class="container">
		<header>
			<h1>Welcome to <strong>VISITA</strong> </h1>
			<div class="support-note">
				<span class="note-ie">Sorry, only modern browsers.</span>
			</div>	
		</header>
		@if(isset($error))
			<h1> {{ $error }} </h1>
			@else
			<h1> AGAY </h1>
		@endif
		<section class="main">
			<form class="form-2" method="POST" action="checkLogin">
				{{ csrf_field() }}
				<h1><span  class="log-in">Log in</span></h1>
				<p class="float">
					
					<input type="text" name="login" placeholder="Username">
				</p>
				<p class="float">
					
					<input type="password" name="password" placeholder="Password" class="showpassword">
				</p>
				<p class="clearfix"> 
					   
					<input style="width: 100%; margin: auto;" type="submit" name="submit" value="Log in">
				</p>
			</form>​​
		</section>
	 </div>

@endsection