@extends('layouts.app')

@section('content')
	<br><br><br>
	<div class="container">
		<table class="table table-striped table-dark">
		  <thead>
		    <tr>
				<th scope="col">First</th>
				<th scope="col">Last</th>
				<th scope="col">Email</th>
				<th scope="col">Contact</th>
				<th scope="col">Country</th>
				<th scope="col">Handle</th>
		    </tr>
		  </thead>
		  <tbody>
		  	@if($users)
		  		@foreach($users as $user)
					<tr>
						<td>{{$user->fname}}</td>
						<td>{{$user->lname}}</td>
						<td>{{$user->email}}</td>
						<td>{{$user->contact}}</td>
						<td>{{$user->country}}</td>
						<td>
							<span>
								<button class="btn btn-danger" type="button" data-toggle="modal" data-target="#exampleModal2" data-aaa="{{$user->id}}" id="targerModal2" >DELETE</button>
								<button class="btn btn-info" type="button" data-toggle="modal" data-target="#exampleModal" 
								data-id="{{$user->id}}"
								data-fname="{{$user->fname}}"
								data-lname="{{$user->lname}}"
								data-email="{{$user->email}}"
								data-contact="{{$user->contact}}"
								data-user_type="{{$user->user_type}}"
								data-country="{{$user->country}}"
								data-city="{{$user->city}}"
								data-add_info="{{$user->add_info}}"
								id="targerModal" >UPDATE</button>
							</span>
						</td>
					</tr>
						<!-- MODAL -->
						<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						  <div class="modal-dialog" role="document">
						    <div class="modal-content">
						      <div class="modal-header">
						        <h5 class="modal-title">Are you sure?</h5>
						        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						          <span aria-hidden="true">&times;</span>
						        </button>
						      </div>
						      <div class="modal-body text-center">
								<form class="toDelete" method="POST" action="{{ route('users.destroy', [$user->id]) }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}

							        <button type="submit" class="btn btn-warning">YES</button>
							        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
								</form>
						      </div>
							      <div class="modal-footer">
							      </div>
						    </div>
						  </div>
						</div>
					</div>
			    @endforeach
		    @else
		    	<tr>
		    		<td rowspan="5">NO USERS</td>
		    	</tr>
		    @endif
		  </tbody>
		</table>
		<button class="btn btn-primary"><a href="/register" style="text-decoration: none; color: white;">ADD USER</a></button>
		<div style="display: inline-block; float: right;">{{ $users->links() }}</div>

		<!-- MODAL -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Update Form</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
				<form class="toUpdate" method="put" action="{{ route('users.update' , [$user->id]) }}">
					<input name="_method" type="hidden" value="PATCH">
					{{ csrf_field() }}
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputFname">First Name</label>
							<input type="text" class="form-control" id="inputFname" placeholder="First name" name="fname" required>
						</div>
						<div class="form-group col-md-6">
							<label for="inputLname">Last Name</label>
							<input type="text" class="form-control" id="inputLname" placeholder="Last name" name="lname" required>
						</div>
					</div>

					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4">Email</label>
							<input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" required>
						</div>
						<div class="form-group col-md-6">
							<label for="inputPassword4">Password</label>
							<input type="password" class="form-control" id="inputPassword4" placeholder="Password" name="password" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputFname">Contact</label>
							<input type="number" class="form-control" id="inputContact" name="contact" required>
						</div>
						<div class="form-group col-md-6">
							<label for="inputLname">Country</label>
							<input type="text" class="form-control" id="inputCountry" name="country" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputCity">City</label>
							<input type="text" class="form-control" id="inputCity" name="city" required>
						</div>
						<div class="form-group col-md-4">
							<label for="inputState">User Type</label>
							<select id="inputState" class="form-control" name="userType" required>
							<option value="1">Customer</option>
							<option value="0">Tourist Guide</option>
							</select>
						</div>
					</div>
					<div class="form-group">
					    <label for="exampleFormControlTextarea1">Additional Info</label>
					    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="addInfo" required></textarea>
			  		</div>
		      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary">Submit</button>
			      </div>
				</form>
		    </div>
		  </div>
		</div>
	</div>


<script type="text/javascript">
	$(document).on("click", "#targerModal", function (){
		var id = $(this).data('id');
		var fname = $(this).data('fname');
		var lname = $(this).data('lname');
		var email = $(this).data('email');
		var contact = $(this).data('contact');
		var user_type = $(this).data('user_type');
		var country = $(this).data('country');
		var city = $(this).data('city');
		var add_info = $(this).data('add_info');


		$('.toUpdate').attr('action', "users/update/"+id);

		$('input[name="fname"]').val(fname);
		$('input[name="lname"]').val(lname);
		$('input[name="email"]').val(email);
		$('input[name="contact"]').val(contact);
		$('input[name="userType"]').val(user_type);
		$('input[name="country"]').val(country);
		$('input[name="city"]').val(city);
		$('input[name="addInfo"]').val(add_info);

	});

	$(document).on("click", "#targerModal2", function (){
		var id = $(this).data('aaa');
		console.log(id);
		// var actionVal = "{{ route('users.destroy', "+id+") }}";
		$('input[name="delete"]').val(id);
		$('.toDelete').attr('action', "users/"+id);
	});
</script>

@endsection