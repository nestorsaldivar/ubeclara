@extends('layouts.app')

@section('content')
<div class="card-header">{{ __('Register') }}</div>
    <div class="container" style="margin-top: 50px;">
        <form method="POST" action="{{ route('register') }}"> <!-- REGISTERCONTROLLER -->
            @csrf
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputFname">First Name</label>
                    <input type="text" class="form-control" id="inputFname" placeholder="First name" name="fname" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputLname">Last Name</label>
                    <input type="text" class="form-control" id="inputLname" placeholder="Last name" name="lname" required>
                </div>
            </div> 
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Email</label>
                    <input type="email" class="form-control" id="inputEmail4" placeholder="Email" name="email" value="{{ old('email') }}"required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Password</label>
                    <input type="password" class="form-control" id="inputPassword1" placeholder="Password" name="password" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Confirm Password</label>
                    <input type="password" class="form-control" id="inputPassword2" placeholder="Password" name="password_confirmation" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputFname">Contact</label>
                    <input type="number" class="form-control" id="inputContact" name="contact" required>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputLname">Country</label>
                    <input type="text" class="form-control" id="inputCountry" name="country" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputCity">City</label>
                    <input type="text" class="form-control" id="inputCity" name="city" required>
                </div>
                <div class="form-group col-md-6">
                    <input type="hidden" value="1" class="form-control" id="inputCountry" name="userType" required>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Additional Info</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="addInfo" required></textarea>
            </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                  @endif
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
        </form>
    </div>  

<!--    <form action="store" method="POST">
        {{ csrf_field() }}
        <input type="text" name="">
        <input type="submit" name="submit" value="submit">
    </form> -->
@endsection