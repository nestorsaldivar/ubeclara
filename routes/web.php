<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/login', 'PagesController@login');

Route::post('users/store', 'UsersController@store')->name('users.store');
Route::get('users/create', 'UsersController@create'); 
Route::get('users', 'HomeController@dashboard');
Route::get('users/update/{id}', 'HomeController@update')->name('users.update');
Route::delete('/users/{id}', 'HomeController@destroy')->name('users.destroy');
// Route::resource('/users', 'UsersController');
// app/http/controllers


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
